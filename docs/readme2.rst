Gestion du projet
=================

Le projet est géré selon la méthodologie `Hermes`_.

.. _Hermes: http://www.hermes.admin.ch/

Dates clé
---------

+-----------------+--------------------------+------------+
| Phase           | Prérequis                | Livrables  |
+=================+==========================+============+
| Initialisation  | Mandat de projet signé   |            |
+-----------------+--------------------------+------------+
| Conception      | Mandat de projet signé   |            |
+-----------------+--------------------------+------------+
| Réalisation     | Mandat de projet signé   |            |
+-----------------+--------------------------+------------+
| Déploiement     | Mandat de projet signé   |            |
+-----------------+--------------------------+------------+

- Be awesomex
- Make things faster

Installation
------------

Install $project by running:

    install project

Contribute
----------

- Issue Tracker: github.com/$project/$project/issues
- Source Code: github.com/$project/$project

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@google-groups.com

License
-------

The project is licensed under the BSD license.
